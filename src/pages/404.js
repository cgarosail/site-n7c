import React from "react"
import Footer from "../components/Footer"

const NotFoundPage = () => (

  <section className="hero is-fullheight is-mobile is-primary">
    <div className="hero-head">
      <nav className="navbar">
        <div className="container">
          <div className="navbar-brand">
            <a className="navbar-item has-text-warning" href="/">
              <h2 clasName="title">N7 Consulting</h2>
            </a>
          </div>
        </div>
      </nav>
    </div>
    <div className="hero-body">
      <div className="container">
        <h1 className="title has-text-warning">
          Oups !
        </h1>
        <h2 className="subtitle">
          La page que vous recherchez semble introuvable !
        </h2>
        <h3 className="subtitle">
          <a href="/">Retour à la page d'accueil</a>
        </h3>
      </div>
    </div>

    <div className="hero-foot">
      <Footer/>
    </div>
  </section>

)

export default NotFoundPage
