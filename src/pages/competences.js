import React from 'react'


import Footer from '../components/Footer'
import NavbarMenu from "../components/NavbarMenu"
import {useStaticQuery, graphql} from "gatsby"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faMobileAlt, faDesktop, faNetworkWired, faBrain, faQuestion } from '@fortawesome/free-solid-svg-icons'

function Competences() {
  
  const data = useStaticQuery(graphql`
      query Navbar {
          site {
          siteMetadata {
              author
              navbar {
                title
                options {
                  deroulant
                  link
                  name
                  dl
                  subs {
                    link
                    name
                  }
                }
              }
              pages {
              Accueil {
                  PresentationJE {
                  titre
                  paragraphes {
                      texte
                      titre
                  }
                  }
                  Statistiques {
                  titre
                  stats {
                      titre
                  }
                  }
              }
              }
              title
          }
          }
      }
      
      
    `)
    

  return (
    <div>
        <section className="hero is-white is-medium">
            <div className="hero-head">
                <NavbarMenu siteData={data.site.siteMetadata.navbar} />
            </div>
            <div className="hero-body">

                <section className="hero is-white is-middle">
                    <div className="hero-head">
                            <h1 className="title has-text-warning has-text-centered">Sciences du numérique</h1>
                    </div>
                    <div className="hero-body">
                        <div className="columns is-multiline">
                            <div className="column">
                                <div className="content notification is-one-third is-danger">
                                    <div className="image has-text-centered">
                                        <FontAwesomeIcon size="5x" icon={faDesktop} color="white" />
                                    </div>
                                    <h1 className="title has-text-white">Développement web</h1>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam nec augue condimentum, eleifend neque in, dapibus ipsum. Quisque posuere consequat felis. Praesent quis nunc a ligula mollis laoreet. Donec quis sem metus. Vestibulum tincidunt libero ac velit sollicitudin aliquet. Aenean condimentum fermentum felis eget tristique. Nunc lacus erat, pellentesque sit amet tincidunt at, finibus vitae elit. Nunc elit metus, ultricies in justo non, molestie fringilla odio.</p>
                                </div>
                            </div>
                            <div className="column">
                                <div className="content notification is-one-third is-danger">
                                    <div className="image has-text-centered">
                                        <FontAwesomeIcon size="5x" icon={faMobileAlt} color="white" />
                                    </div>
                                    <h1 className="title has-text-white">Développement applications mobile</h1>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam nec augue condimentum, eleifend neque in, dapibus ipsum. Quisque posuere consequat felis. Praesent quis nunc a ligula mollis laoreet. Donec quis sem metus. Vestibulum tincidunt libero ac velit sollicitudin aliquet. Aenean condimentum fermentum felis eget tristique. Nunc lacus erat, pellentesque sit amet tincidunt at, finibus vitae elit. Nunc elit metus, ultricies in justo non, molestie fringilla odio.</p>
                                </div>
                            </div>
                            <div className="column">
                                <div className="content notification is-one-third is-danger">
                                    <div className="image has-text-centered">
                                        <FontAwesomeIcon size="5x" icon={faNetworkWired} color="white" />
                                    </div>
                                    <h1 className="title has-text-white">Réseaux</h1>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam nec augue condimentum, eleifend neque in, dapibus ipsum. Quisque posuere consequat felis. Praesent quis nunc a ligula mollis laoreet. Donec quis sem metus. Vestibulum tincidunt libero ac velit sollicitudin aliquet. Aenean condimentum fermentum felis eget tristique. Nunc lacus erat, pellentesque sit amet tincidunt at, finibus vitae elit. Nunc elit metus, ultricies in justo non, molestie fringilla odio.</p>
                                </div>
                            </div>
                            <div className="column">
                                <div className="content notification is-one-third is-danger">
                                    <div className="image has-text-centered">
                                        <FontAwesomeIcon size="5x" icon={faBrain} color="white" />
                                    </div>
                                    <h1 className="title has-text-white">Intelligence artificielle</h1>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam nec augue condimentum, eleifend neque in, dapibus ipsum. Quisque posuere consequat felis. Praesent quis nunc a ligula mollis laoreet. Donec quis sem metus. Vestibulum tincidunt libero ac velit sollicitudin aliquet. Aenean condimentum fermentum felis eget tristique. Nunc lacus erat, pellentesque sit amet tincidunt at, finibus vitae elit. Nunc elit metus, ultricies in justo non, molestie fringilla odio.</p>
                                </div>
                            </div>
                        </div>
                        <div id="elec"></div>
                    </div>
                </section>
                
                <section className="hero is-white is-middle"  >
                    <div className="hero-head" >
                            <h1 className="title has-text-warning has-text-centered">Electronique</h1>
                    </div>
                    <div className="hero-body">
                        <div className="columns is-multiline">
                            <div className="column">
                                <div className="content notification is-one-third is-success">
                                    <div className="image has-text-centered">
                                        <FontAwesomeIcon size="5x" icon={faQuestion} color="white" />
                                    </div>
                                    <h1 className="title has-text-white">Lorem Ipsum</h1>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam nec augue condimentum, eleifend neque in, dapibus ipsum. Quisque posuere consequat felis. Praesent quis nunc a ligula mollis laoreet. Donec quis sem metus. Vestibulum tincidunt libero ac velit sollicitudin aliquet. Aenean condimentum fermentum felis eget tristique. Nunc lacus erat, pellentesque sit amet tincidunt at, finibus vitae elit. Nunc elit metus, ultricies in justo non, molestie fringilla odio.</p>
                                </div>
                            </div>
                            <div className="column">
                                <div className="content notification is-one-third is-success">
                                    <div className="image has-text-centered">
                                        <FontAwesomeIcon size="5x" icon={faQuestion} color="white" />
                                    </div>
                                    <h1 className="title has-text-white">Lorem Ipsum</h1>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam nec augue condimentum, eleifend neque in, dapibus ipsum. Quisque posuere consequat felis. Praesent quis nunc a ligula mollis laoreet. Donec quis sem metus. Vestibulum tincidunt libero ac velit sollicitudin aliquet. Aenean condimentum fermentum felis eget tristique. Nunc lacus erat, pellentesque sit amet tincidunt at, finibus vitae elit. Nunc elit metus, ultricies in justo non, molestie fringilla odio.</p>
                                </div>
                            </div>
                            <div className="column">
                                <div className="content notification is-one-third is-success">
                                    <div className="image has-text-centered">
                                        <FontAwesomeIcon size="5x" icon={faQuestion} color="white" />
                                    </div>
                                    <h1 className="title has-text-white">Lorem Ipsum</h1>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam nec augue condimentum, eleifend neque in, dapibus ipsum. Quisque posuere consequat felis. Praesent quis nunc a ligula mollis laoreet. Donec quis sem metus. Vestibulum tincidunt libero ac velit sollicitudin aliquet. Aenean condimentum fermentum felis eget tristique. Nunc lacus erat, pellentesque sit amet tincidunt at, finibus vitae elit. Nunc elit metus, ultricies in justo non, molestie fringilla odio.</p>
                                </div>
                            </div>
                            <div className="column">
                                <div className="content notification is-one-third is-success">
                                    <div className="image has-text-centered">
                                        <FontAwesomeIcon size="5x" icon={faQuestion} color="white" />
                                    </div>
                                    <h1 className="title has-text-white">Lorem Ipsum</h1>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam nec augue condimentum, eleifend neque in, dapibus ipsum. Quisque posuere consequat felis. Praesent quis nunc a ligula mollis laoreet. Donec quis sem metus. Vestibulum tincidunt libero ac velit sollicitudin aliquet. Aenean condimentum fermentum felis eget tristique. Nunc lacus erat, pellentesque sit amet tincidunt at, finibus vitae elit. Nunc elit metus, ultricies in justo non, molestie fringilla odio.</p>
                                </div>
                            </div>
                            <div className="column">
                                <div className="content notification is-one-third is-success">
                                    <div className="image has-text-centered">
                                        <FontAwesomeIcon size="5x" icon={faQuestion} color="white" />
                                    </div>
                                    <h1 className="title has-text-white">Lorem Ipsum</h1>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam nec augue condimentum, eleifend neque in, dapibus ipsum. Quisque posuere consequat felis. Praesent quis nunc a ligula mollis laoreet. Donec quis sem metus. Vestibulum tincidunt libero ac velit sollicitudin aliquet. Aenean condimentum fermentum felis eget tristique. Nunc lacus erat, pellentesque sit amet tincidunt at, finibus vitae elit. Nunc elit metus, ultricies in justo non, molestie fringilla odio.</p>
                                </div>
                            </div>
                        </div>
                        <div id="hydro"></div>
                    </div>
                </section>



                <section className="hero is-white is-middle">
                    <div className="hero-head">
                            <h1 className="title has-text-warning has-text-centered">Hydraulique</h1>
                    </div>
                    <div className="hero-body">
                        <div className="columns is-multiline">
                            <div className="column">
                                <div className="content notification is-one-third is-link">
                                    <div className="image has-text-centered">
                                        <FontAwesomeIcon size="5x" icon={faQuestion} color="white" />
                                    </div>
                                    <h1 className="title has-text-white">Lorem Ipsum</h1>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam nec augue condimentum, eleifend neque in, dapibus ipsum. Quisque posuere consequat felis. Praesent quis nunc a ligula mollis laoreet. Donec quis sem metus. Vestibulum tincidunt libero ac velit sollicitudin aliquet. Aenean condimentum fermentum felis eget tristique. Nunc lacus erat, pellentesque sit amet tincidunt at, finibus vitae elit. Nunc elit metus, ultricies in justo non, molestie fringilla odio.</p>
                                </div>
                            </div>
                            <div className="column">
                                <div className="content notification is-one-third is-link">
                                    <div className="image has-text-centered">
                                        <FontAwesomeIcon size="5x" icon={faQuestion} color="white" />
                                    </div>
                                    <h1 className="title has-text-white">Lorem Ipsum</h1>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam nec augue condimentum, eleifend neque in, dapibus ipsum. Quisque posuere consequat felis. Praesent quis nunc a ligula mollis laoreet. Donec quis sem metus. Vestibulum tincidunt libero ac velit sollicitudin aliquet. Aenean condimentum fermentum felis eget tristique. Nunc lacus erat, pellentesque sit amet tincidunt at, finibus vitae elit. Nunc elit metus, ultricies in justo non, molestie fringilla odio.</p>
                                </div>
                            </div>
                            <div className="column">
                                <div className="content notification is-one-third is-link">
                                    <div className="image has-text-centered">
                                        <FontAwesomeIcon size="5x" icon={faQuestion} color="white" />
                                    </div>
                                    <h1 className="title has-text-white">Lorem Ipsum</h1>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam nec augue condimentum, eleifend neque in, dapibus ipsum. Quisque posuere consequat felis. Praesent quis nunc a ligula mollis laoreet. Donec quis sem metus. Vestibulum tincidunt libero ac velit sollicitudin aliquet. Aenean condimentum fermentum felis eget tristique. Nunc lacus erat, pellentesque sit amet tincidunt at, finibus vitae elit. Nunc elit metus, ultricies in justo non, molestie fringilla odio.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </section>
        <Footer/>
    </div>
  )
}

export default Competences