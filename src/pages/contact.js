import React from 'react'
import Layout from '../components/layout';
import ContactForm from '../components/ContactForm';
import SocialMedia from '../components/SocialMedia';

const Contact = () => {
    return (
        <Layout>
            <section className="columns first">
                <div className="column is-half">
                    <SocialMedia/>
                </div>
                <div className="column is-one-third">
                    <ContactForm/>
                </div>
            </section>
        </Layout>
    )
}

 export default Contact