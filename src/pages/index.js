import React from 'react'

import Stats from '../components/Stats'
import PresentationJE from '../components/PresentationJE';
import DomainesGeneral from '../components/DomainesGeneral';
import Layout from '../components/layout';
import FirstImpressionV2 from '../components/FirstImpressionV2';

function IndexPage() {
  
  return (
          <Layout>
            <FirstImpressionV2 />
            <PresentationJE />
            <Stats />
            <DomainesGeneral/>
          </Layout>
  )
}

export default IndexPage