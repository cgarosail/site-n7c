import React from 'react'

import DomainesGeneral from '../components/DomainesGeneral';
import PresentationEtude from '../components/PresentationEtude';
import ExempleEtudes from '../components/ExempleEtudes';
import Layout from '../components/layout';
import PresentationCompleteJE from '../components/PresentationCompleteJE';

const Entrepise = () => {
    return (
        <Layout>
            <PresentationCompleteJE/>
            <PresentationEtude />
            <DomainesGeneral />
            <ExempleEtudes />
        </Layout>
    )
}

 export default Entrepise