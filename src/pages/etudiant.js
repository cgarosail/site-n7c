import React from 'react'

import PresentationCompleteJE from '../components/PresentationCompleteJE'
import Layout from '../components/layout'
import PresentationEtude from '../components/PresentationEtude';
import AvantagesEtude from '../components/AvantagesEtude'
const Etudiant = () => {
    return(
        <Layout>
            
            <PresentationCompleteJE/>
            <PresentationEtude/>
            <AvantagesEtude/>
            {/*
            <PossibilitesEtude/> */}
            <h1>Bienvenue sur la page destinée aux étudiants</h1>
            <p style={{color:"red", fontSize:"200%"}}>/!\ Cette page est encore en construction /!\</p>
            <p>Il y aura sur cette page :</p>
            <ul>
                <li>Explication de ce qu’est une JE</li>
                <li>Une explication du déroulement d'une étude</li>
                <li>Ce que la JE peut apporter à un étudiant</li>
                <li>Les possibilités d'étude</li>
            </ul>
        </Layout>    
    )
}

export default Etudiant