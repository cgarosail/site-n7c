import React from 'react'



import CNJE from '../images/JE_logo.png'
import N7C from '../images/Logo_transparent.png'
function PresentationCompleteJE() {

    return(
        <section className="hero first">

            <div className="hero-body">
                <div className="container">
                    <section className="hero is-transparent text-invert">
                        <div className="hero-head">
                            <div className="container">
                                <h1 className="title is-size-3 has-text-warning has-text-centered" >
                                Qu'est ce qu'une Junior Entreprise ?
                                </h1>
                            </div>
                            </div>
                            <div className="hero-body">
                            <div className="columns">
                                <figure className="column is-one-fifth">
                                <p className="image 128x128px">
                                    <img src={CNJE} alt="CNJE"/>
                                </p>

                                </figure>
                                <div className="column">
                                <div className="content is-medium">
                                    <h4 className="title is-size-4 has-text-link">Le concept</h4>
                                    <p>
                                        Les Junior-Entreprises sont des associations à vocation économique et pédagogique, à but non lucratif. 
                                        Implantées au sein des écoles et universités, elles permettent aux étudiants de mettre en pratique l'enseignement théorique dont ils bénéficient, en réalisnat des études correspondant aux domaines de compétences de leur école, pour des clients très variés : entrepreneurs, très petites entreprises, petites et moyennes entreprises, grands groupes, associations, collectivités, etc.
                                        Sur le modèle des cabinets de conseil, elles assurent à leurs administrateurs une formation avant l'heure, aux mécanismes de la gestion d'une entreprise et de management d'une équipe, à leurs intervenants une première expérience professionnelle concrète.
                                        <br/>
                                        Les clients bénéficient quant à eux des capacités d'innovation des étudiants, de leur dynamisme, de leur aptitude à mettre en oeuvre des projets d'ampleur en mobilisant un grand nombre d'intervenants, du soutien pédagogique des enseignants et chercheurs des écoles... Ils sont en outre assurés de bénéficier d'études de qualité, les associations étant contrôlées chaque année.
                                    </p>

                                </div>
                            </div>
                            </div>
                            <div className="columns">
                                <div className="column">
                                    <div className="content is-medium">
                                        <h4 className="title is-size-4 has-text-link">Les avantages pour un professionel</h4>
                                        <p>
                                            Si les Junior-Entreprises sont autant solicitées par les professionels, c'est en partie du fait de leur statut d'association qui permet d'être flexibles et réactives. Le client est en contact avec un membre de la structure, interlocuteur unique qui l'accompagne et le conseille dans le déroulement de la prestation. De plus les Junior-Entreprises sont à but non lucratif ce qui leur permet d'offrir des tarifs très compétitifs.                                        
                                        </p>
                                        <p>
                                        Elles sont aujourd'hui en France plus de 200, fédérées par la <strong>Confédération Nationale des Junior-Entrepreneurs</strong> regroupant plus de 20 000 étudiants. Elles sont également implantées dans d'autres pays.
                                        </p>
                                    </div>
                                </div>
                                <figure className="column is-one-fifth">
                                    <p className="image 128x128">
                                    <img src={N7C} alt="N7 Consulting"/>
                                    </p>
                                </figure>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </section>

    )
}

export default PresentationCompleteJE