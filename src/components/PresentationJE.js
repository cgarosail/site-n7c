import React from 'react'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faStopwatch, faHandsHelping, faWallet } from '@fortawesome/free-solid-svg-icons'
import { faGem } from '@fortawesome/free-regular-svg-icons'
import CNJE from '../images/JE_logo.png'
import {Link} from 'gatsby'
function PresentationJE() {

  return(
    <div>




      <section className="hero">

        <div className="hero-body  first"   >
          <div className="container">
            <section className="hero is-transparent text-invert">
              <div className="hero-head">
                <div className="container">
                  <h1 className="title is-size-3 has-text-warning has-text-centered">
                    42 ans d'expertise au service d'un monde innovant
                  </h1>
                </div>
              </div>
              <div className="hero-body">

                <div className="content is-medium has-text-primary">
                  Forte de 42 ans d’expérience, la Junior Entreprise de l’ENSEEIHT figure aujoud’hui parmi les 30 meilleurs Junior-Entreprises françaises. Elle s’appuie sur l’expertise de plus de 1600 élèves performants et sur la réputation de son école centenaire assurant une formation technique exigeante.
                </div>
              </div>
            </section>
          </div>
        </div>
      </section>














      <section className="hero">

        <div className="hero-body ">
          <div className="container" >
            <section className="hero is-transparent text-invert">
              <div className="hero-head">
                <div className="container has-text-centered">
                  <h1 className="title is-size-3 has-text-warning has-text-centered" >
                    Nos atouts 
                  </h1>
                </div>
              </div>
              <div className="hero-body  has-text white" >
              <div className="columns has-text-primary">

                <div className="column">

                  <div className="content has-text-centered">
                    <image className="image">
                      <FontAwesomeIcon size="4x" icon={faStopwatch} color="primary" />
                    </image>
                  </div>

                  <div className="content has-text-centered">
                    <h4 className="title is-4 has-text-warning">Réactivité</h4>
                    <p>Nous nous engageons à <strong>répondre</strong> à vos demandes <strong>sous 24h.</strong>  </p>
                  </div>

                </div>
                <div className="column"><div className="content has-text-centered">
                    <image className="image">
                      <FontAwesomeIcon size="4x" icon={faHandsHelping} color="primary" />
                    </image>
                  </div>

                  <div className="content has-text-centered">
                    <h4 className="title is-4 has-text-warning">Accompagnement professionnel</h4>
                    <p>Un <strong>chef de projet</strong> est <strong>à votre écoute</strong> et <strong>vous tient informé</strong> tout au long <strong>de l'avencement de l'étude.</strong> </p>
                  </div>
                  
                </div>
                <div className="column">
                  <div className="content has-text-centered">
                    <image className="image">
                      <FontAwesomeIcon size="4x" icon={faGem} color="primary" />
                    </image>
                  </div>

                  <div className="content has-text-centered">

                    <h4 className="title is-4 has-text-warning">Qualité</h4>

                    <p>Nous sommes <strong>soucieux de votre satisfaction</strong> et organisons nos processus en conséquence.
                    <strong>Un responsable qualité</strong> assure que les <strong>exigences soient respectées</strong> sur toutes les phases de l'étude, de la prise de contact au rendu livrable.</p>
                  </div>
                </div>
                <div className="column">

                  <div className="content has-text-centered">
                    <image className="image">
                      <FontAwesomeIcon size="4x" icon={faWallet} color="primary" />
                    </image>
                  </div>

                  <div className="content has-text-centered">
                    <h4 className="title is-4 has-text-warning">Compétivité</h4>
                    <p >Vous bénéficiez de <strong>tarifs avantageux</strong> sur no prestations grâce au statut dérogatoire des Junior-Entreprises.</p>
                  </div>
                  
                </div>
              </div>

              </div>
            </section>
          </div>
        </div>
      </section>











    






















































      <section className="hero">

        <div className="hero-body ">
        <div className="container">
          <section className="hero is-transparent text-invert">
            <div className="hero-head">
              <div className="container">
                <h1 className="title is-size-3 has-text-warning has-text-centered">
                  Qu'est ce qu'une Junior Entreprise ?
                </h1>
              </div>
            </div>
            <div className="hero-body  ">
              <div className="columns">
                <figure className="column is-one-fifth">
                  <p className="image 128x128px">
                    <img src={CNJE} alt="CNJE"/>
              
                  </p>
                </figure>
                <div className="column">
                  <div className="content is-medium">
                    <p>
                      Une <strong>Junior-Entreprise</strong> est une association loi 1901, implantée au sein d'un établissement supérieur, qui fonctionne sur le modèle d'un <strong>cabinet de conseil.</strong>
                    </p>
                    <p>
                      Elle permet aux <strong>étudiants</strong> de mettre en pratique leur enseignements théoriques en réalisant des <strong>missions rémunérées</strong> pour le compte de professionnels.
                    </p>
                    <p>
                      Elles sont aujourd'hui en France plus de 200, fédérées par la <strong>Confédération Nationale des Junior-Entrepreneurs</strong> regroupant plus de 20 000 étudiants. Elles sont également implantées dans d'autres pays.
                    </p>
                    <p className="content has-text-right">

                      <Link to='/entreprise'><button className="button is-danger">En savoir plus</button></Link>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </section>























  </div>

  )
}

export default PresentationJE