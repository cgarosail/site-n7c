import React from 'react';


const BulmaCarousel = (() => {
    if (typeof window !== 'undefined') {
        return require('bulma-carousel')
    }
})()

class Carousel extends React.PureComponent {

    componentDidMount() {
        BulmaCarousel.attach('.hero-carousel', { slidesToShow: 1, loop: true, infinite: true, autoplay: true, timing: 'ease-out' });
    }
    render() {
        return (
            <section className="hero has-carousel carousel-medium is-medium" >
                <div className="hero-carousel" >
                    <div className="item-3 has-background-link is-medium carousel-medium">
                        <div className="hero-body">
                            <h3 className="title is-size-6 is-size-3-fullhd is-size-4-desktop">42 ans d’expérience</h3>
                        </div>
                    </div>

                    <div className="item-3 hero has-background-danger is-medium carousel-medium">
                        <div className="hero-body">
                            <h3 className="title is-size-6 is-size-3-fullhd is-size-4-desktop">Prix de la meilleure étude en ingenierie en 2012</h3>
                        </div>
                    </div>

                    <div className="item-3 hero has-background-warning is-medium carousel-medium">
                        <div className="hero-body">
                            <h3 className="title is-size-6 is-size-3-fullhd is-size-4-desktop">Prix du meilleur espoir en 2010</h3>
                        </div>
                    </div> 

                    <div className="item-3 has-background-link is-medium carousel-medium">
                        <div className="hero-body">
                            <h3 className="title is-size-6 is-size-3-fullhd is-size-4-desktop">Parmis les 30 meilleurs Junior Entreprises</h3>
                        </div>
                    </div> 

                    <div className="item-3 has-background-danger is-medium carousel-medium">
                        <div className="hero-body">
                            <h3 className="title is-size-6 is-size-3-fullhd is-size-4-desktop">Plus de 500 études réalisées</h3>
                        </div>
                    </div> 

                    <div className="item-3 has-background-warning is-medium carousel-medium">
                        <div className="hero-body">
                            <h3 className="title is-size-6 is-size-3-fullhd is-size-4-desktop">99.999% satisfaction client</h3>
                        </div>
                    </div> 


                </div>
                <div className="hero-head"></div>
                <div className="hero-body"></div>
                <div className="hero-foot"></div>
            </section>

        

        );
    }

}
export default Carousel