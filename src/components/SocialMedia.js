import React from "react"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFacebook, faTwitter, faLinkedin} from '@fortawesome/free-brands-svg-icons'



function SocialMedia(){

        return (
            <section className="hero">
                <div className="container">

                    <div className="hero-head">
                        <h2 className="title is-3 has-text-warning">Nous contacter sur nos réseaux</h2>
                    </div>

                    <div className="columns is-multiline is-mobile is-centered is-flex">

                        <a className='column is-full has-text-centered link' href="https://twitter.com/n7consulting"><FontAwesomeIcon size="8x"  icon={faTwitter} color="primary" style={{marginTop: "5%"}} /><h4 className="subtitle is-4 has-text-primary">Notre twitter : n7consulting</h4></a>
                        <a className='column is-full has-text-centered link' href="https://www.facebook.com/N7ConsultingToulouse/"><FontAwesomeIcon size="8x" icon={faFacebook} color="primary" style={{marginTop: "5%"}} /><h4 className="subtitle is-4 has-text-primary">Notre Facebook : N7ConsultingToulouse</h4></a>
                        <a className='column is-full has-text-centered link' href="https://www.linkedin.com/company/n7-consulting"><FontAwesomeIcon size="8x" icon={faLinkedin} color="primary" style={{marginTop: "5%"}} /><h4 className="subtitle is-4 has-text-primary">Notre linkedin : n7-consulting</h4></a>
                    
                    </div>
                </div>
            </section>
        )
}

export default SocialMedia
