import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleUp} from '@fortawesome/free-solid-svg-icons'
import { faFacebook, faTwitter, faLinkedin} from '@fortawesome/free-brands-svg-icons'

import CNJE from '../images/JE_logo.png'
import AMARIS from '../images/amaris.png'
import SOGE from '../images/SOGE.png'

import * as Scroll from 'react-scroll'



class Footer extends React.PureComponent {
    constructor(props) {
        super(props);
        this.dropUp = this.dropUp.bind(this)
    }
      
    dropUp() {
        Scroll.animateScroll.scrollToTop()
      }

    render() {
        return (

            <footer className="footer has-background-primary has-text-grey-lighter">
                <div className="content ">
                    <div className="columns ">
                        <div className="column is-half">
                            <h3 className="title has-text-white">Contactez Nous</h3>
                            <div className="columns is mobile">
                                <div className="column">
                                    <p>Une question, une remarque, un devis ? Contactez nous ! Réponse sour 24h.</p>
                                    <div className="columns" >
                                        <a className='column' href="https://twitter.com/n7consulting"><FontAwesomeIcon size="3x"  icon={faTwitter} color="rgb(255,255,255)" /></a>
                                        <a className='column' href="https://www.facebook.com/N7ConsultingToulouse/"><FontAwesomeIcon size="3x" icon={faFacebook} color="rgb(255,255,255)" /></a>
                                        <a className='column' href="https://www.linkedin.com/company/n7-consulting"><FontAwesomeIcon size="3x" icon={faLinkedin} color="rgb(255,255,255)" /></a>
                                    </div>
                                </div>
                                <div className="column">
                                    <div> 2 rue Charles Camichel, 31071 Toulouse CEDEX7</div>
                                    <div>+33 (0)5 34 32 20 99</div>
                                    <a href="mailto:contact@n7consulting.fr" className="has-text-link">contact@n7consulting.fr</a>
                                    <div>N°SIRET:344 162 490 00017</div>
                                </div>
                            </div>
                        </div>
                        <div className="column is-5">
                            <title className="title has-text-white">Nos partenaires</title>
                            <div className="columns">
                                <div className="column">
                                    <div className="colummns">
                                        <div className="column">
                                            <p className="image is-96x96">
                                                <img src={AMARIS} alt="" href="www.amaris.com"/>
                                            </p>
                                        </div>
                                        <div className="column">
                                            <h2 className="title has-text-white is-size-5">Amaris</h2>
                                        </div>
                                    </div>
                                </div>
                                <div className="column">
                                    <div className="colummns">
                                        <div className="column">
                                            <p className="image is-96x96">
                                                <img src={SOGE} alt="" href="www.societegenerale.fr"/>
                                            </p>
                                        </div>
                                        <div className="column">
                                            <h2 className="title has-text-white is-size-5">Société Générale</h2>
                                        </div>
                                    </div>
                                </div>
                                <div className="column">
                                    <div className="colummns">
                                        <div className="column">
                                            <p className="image is-96x96">
                                                <img src={CNJE} alt="" href="www.junior-entreprises.com"/>
                                            </p>
                                        </div>
                                        <div className="column">
                                            <h2 className="title has-text-white is-size-5">Confédération Nationnale des Junior Entreprises</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="column is-1">
                            <FontAwesomeIcon size="6x" onClick={this.dropUp} icon={faAngleUp} color="rgb(255,255,255)" className="link"/>
                        </div>
                    </div>
                </div>
            </footer>
            
        )
    }
}

 export default Footer