import React from "react"


import Carousel from './Carousel'

function Stats() {

  return(


    <section className="hero">


      <div className="hero-body ">
        <div className="container">
          <section className="hero is-transparent text-invert">
            <div className="hero-head">
              <div className="container">
                <h1 className="title is-size-3 has-text-warning has-text-centered">
                  Notre Junior Entreprise en quelques Chiffres
                </h1>
              </div>
            </div>
            <div className="hero-body has-text-centered">
              <Carousel/>
            </div>
          </section>
        </div>
      </div>
    </section>

  )
  
}

export default Stats
 