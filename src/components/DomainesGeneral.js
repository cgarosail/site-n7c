import React from "react"
import { Link } from "gatsby";


function DomainesGeneral()  {


    return(
      <section className="hero">

        <div className="hero-body">
          <div className="container">
            <section className="hero is-transparent text-invert">
              <div className="hero-head">
                <div className="container">
                  <h1 className="title is-size-3 has-text-warning has-text-centered">
                    Nos prestations
                  </h1>
                </div>
              </div>
              <div className="hero-body  has-text white">

                  <div className="columns ">
                    <div className="column">
                      <div className="content notification is-danger has-text-centered">
                        <Link to="/competences" className="link" >
                          <h2 className="subtitle is-5">Numérique</h2>
                          <div className="content">
                            Mauris nec ultricies urna. Etiam velit nisl, elementum vitae venenatis eget, cursus nunc sed, efficitur erat. Curabitur in dolor lacinia neque aliquam posuere. Cras non diam magna. Sed ac mi vitae odio egestas facilisis.
                          </div>
                        </Link>
                      </div>
                    </div>

                    <div className="column">
                      <div className="content notification is-link has-text-centered">
                        <Link to="/competences/#hydro" className="link">
                          <h2 className="subtitle is-5">Hydraulique</h2>
                          <div className="content">
                            Mauris nec ultricies urna. Etiam velit nisl, elementum vitae venenatis eget, cursus nunc sed, efficitur erat. Curabitur in dolor lacinia neque aliquam posuere. Cras non diam magna. Sed ac mi vitae odio egestas facilisis.
                          </div>
                        </Link>
                      </div>
                    </div>


                    <div className="column ">
                      <div className="content notification is-success has-text-centered">
                        <Link to="/competences/#elec" className="link">
                          <h2 className="subtitle is-5">Electronique</h2>
                          <div className="content">
                            Mauris nec ultricies urna. Etiam velit nisl, elementum vitae venenatis eget, cursus nunc sed, efficitur erat. Curabitur in dolor lacinia neque aliquam posuere. Cras non diam magna. Sed ac mi vitae odio egestas facilisis.
                          </div>
                        </Link>
                      </div>
                    </div>
                  <br/>
                </div>
              </div>
            </section>
          </div>
        </div>
      </section>

    ) 
  
}

export default DomainesGeneral
