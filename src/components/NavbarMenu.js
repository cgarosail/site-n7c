import React from 'react'
import Link from 'gatsby-link'
import 'bootstrap/dist/css/bootstrap.min.css'
import '../styles/header.css'


import {
  Collapse,
  Navbar,
  NavbarToggler,
  Nav,
  //NavbarBrand,
  NavItem,
  //NavLink,
  UncontrolledButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  //DropdownItem
} from 'reactstrap';



class NavbarMenu extends React.PureComponent {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  classeUpdate(event) {
    event.target.className="navbarItem"
  }


  render() {
    const menus = this.props.siteData.options.map(option => 
      option.deroulant ? 
        <UncontrolledButtonDropdown style={{marginBottom:'0', paddingLeft: '50px', color:"white"}} key={option.title} >
          <DropdownToggle  nav className="navbarItem" style={{color:"white"}} caret key={option.name}>{option.name}</DropdownToggle>
          <DropdownMenu style = {{backgroundColor: 'rgb(0, 0, 88)', borderRadius: '15px', position:'absolute'}} size='sm' key={option.subs}>
            {option.subs.map( sousMenu => <Link  className="navbarSub" style ={{display:'flex', justifyContent:'center', margin:'5% 4% 5%'}} to={sousMenu.link} key={sousMenu.name}>{sousMenu.name}</Link>)}

          </DropdownMenu>
        </UncontrolledButtonDropdown>
      :
        option.dl ?
          <NavItem style={{marginBottom:'0'}} key={option.title}>
            <a className="navbarItem" href={option.link} key={option.title} download>{option.name}</a>
          </NavItem>
        :
          <NavItem style={{marginBottom:'0'}} key={option.title}>
            <Link className="navbarItem" to={option.link} key={option.title}>{option.name}</Link>
          </NavItem>
        
    ) 


      return(
      <header>
        <Navbar fixed={"top"} expand="md" style={{backgroundColor: ' rgba(0, 162, 225,0.9)', borderRadius:'50px', margin:'10px'}} light>
          <div className="navbarItem" style={{display:"flex", margin: "auto"}}>

            <Link className="navbarItem" style={{paddingLeft:"0"}} to="/">
              {this.props.siteData.title}
            </Link>
          </div>
          <NavbarToggler onClick={this.toggle} onMouseOver={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" style ={{alignItems:'center', marginBottom:'0'}}navbar>
              {menus}
            </Nav>
          </Collapse>
        </Navbar>
      </header>

      )
    }
  } 
  export default NavbarMenu