import React from "react"
import * as Scroll from 'react-scroll'

import AnimatedObject from './AnimatedObject'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faLaptopCode, faPlug, faBroadcastTower, faShip} from '@fortawesome/free-solid-svg-icons'

const BulmaCarousel = (() => {
  if (typeof window !== 'undefined') {
      return require('bulma-carousel')
  }
})()


class FirstImpressionV2 extends React.PureComponent {
  constructor(props) {
    super(props)

    this.state={
      isVisible: false
    }
    this.update = this.update.bind(this)
  }
  
  dropDown () {
    Scroll.animateScroll.scrollTo(window.innerHeight - 20)
  }
  
  update() {
    console.log(this.state.isVisible)
    this.setState({
      isVisible: !this.state.isVisible
    })
    
  }
  componentDidMount() {
    var carousels = BulmaCarousel.attach('.hero-carousel', { slidesToShow: 1, loop: true, infinite: true, autoplay: true, effect: 'translate', autoplaySpeed: "8000" })
    this.setState({ isVisible: true})
    
    carousels[0].on('show', this.update);
  }

    render() {

      return(
        <section className="hero is-fullheight has-carousel is-desktop">
          <div className="hero-carousel is-fullheight" id='carousel'>
            <div className="item-1">
              <div className="hero has-background-grey is-fullheight">
                <div className="hero-body has-text-centered">
                  <div className="container has-text-centered">
                    <div className="columns">
                      <div className="column">
                        <AnimatedObject className="title is-size-4 is-size-2-fullhd is-size-3-desktop has-text-white" delaiPlus="0" etat={this.state.isVisible ? 'open' : 'closed'}>Dynamisme</AnimatedObject>
                      </div>
                      <div className="column">
                        <AnimatedObject className="title is-size-4 is-size-2-fullhd is-size-3-desktop has-text-white" delaiPlus="50" etat={this.state.isVisible ? 'open' : 'closed'}>Disponibilité</AnimatedObject>
                      </div>
                      <div className="column">
                        <AnimatedObject className="title is-size-4 is-size-2-fullhd is-size-3-desktop has-text-white" delaiPlus="100" etat={this.state.isVisible ? 'open' : 'closed'}>Savoir-Faire</AnimatedObject>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="item-2">
              <div className="hero has-background-grey is-fullheight">
                <div className="hero-body">
                  <div className="container has-text-centered">
                    <div className="container has-text-left">
                      <AnimatedObject className="title is-size-4 is-size-2-fullhd is-size-3-desktop has-text-white" delaiPlus="0" etat={!this.state.isVisible ? 'open' : 'closed'}>N7 Consulting depuis 1977 au service de ses clients</AnimatedObject>
                      <br/>
                      <AnimatedObject className="has-text-link" delaiPlus="30" etat={!this.state.isVisible ? 'open' : 'closed'}><FontAwesomeIcon size="2x" icon={faLaptopCode}/></AnimatedObject>
                      <AnimatedObject className="subtitle is-size-5 is-size-4-fullhd is-size-4-desktop has-text-white is-spaced" delaiPlus="30" etat={!this.state.isVisible ? 'open' : 'closed'}>Web et Logiciel</AnimatedObject>
                      <br/>
                      <AnimatedObject className="has-text-link" delaiPlus="60" etat={!this.state.isVisible ? 'open' : 'closed'}><FontAwesomeIcon size="2x" icon={faPlug}/></AnimatedObject>
                      <AnimatedObject className="subtitle is-size-5 is-size-4-fullhd is-size-4-desktop has-text-white is-spaced" delaiPlus="60" etat={!this.state.isVisible ? 'open' : 'closed'}>Electronique et Automatique</AnimatedObject>
                      <br/>
                      <AnimatedObject className="has-text-link" delaiPlus="90" etat={!this.state.isVisible ? 'open' : 'closed'}><FontAwesomeIcon size="2x" icon={faBroadcastTower}/></AnimatedObject>
                      <AnimatedObject className="subtitle is-size-5 is-size-4-fullhd is-size-4-desktop has-text-white is-spaced" delaiPlus="90" etat={!this.state.isVisible ? 'open' : 'closed'}>Télécomunications</AnimatedObject>
                      <br/>
                      <AnimatedObject className="has-text-link" delaiPlus="120" etat={!this.state.isVisible ? 'open' : 'closed'}><FontAwesomeIcon size="2x" icon={faShip}/></AnimatedObject>
                      <AnimatedObject className="subtitle is-size-5 is-size-4-fullhd is-size-4-desktop has-text-white is-spaced" delaiPlus="120" etat={!this.state.isVisible ? 'open' : 'closed'}>Hydraulique et Thermique</AnimatedObject>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="hero-head"></div>
          <div className="hero-body"></div>
          <div className="hero-foot">test</div>
        </section>
      )
  }
} 
    


export default FirstImpressionV2
