import React from "react"
import FadeUpComponent from "./PresentationEtudeComponents/FadeUpComponent";




function PresentationEtude() {

    return(
      <div className="timeline is-centered is-size-4 has-text-primary">
        <div className="timeline-item is-primary">
          <div className="timeline-marker is-link is-image is-32x32"></div>
          <FadeUpComponent>
            <div className="timeline-content">
                <p className="title is-4 has-text-warning">Premier contact</p>
                <p>Le client viens nous voir avec un besoin</p>
            </div>
          </FadeUpComponent>
        </div>
        <div className="timeline-item is-primary">
          <span className="timeline-marker is-link is-image is-32x32"></span>
          <FadeUpComponent>
            <div className="timeline-content">
              <p className="title is-4 has-text-warning">Réalisation du devis</p>
              <p>Le client nous donne un cahier des charges et nous définit précisément son besoin et nous chiffrons en conséquence</p>
            </div>
          </FadeUpComponent>
        </div>
        <div className="timeline-item is-primary">
          <div className="timeline-marker is-link is-image is-32x32"></div>
          <FadeUpComponent>
            <div className="timeline-content">
              <p className="title is-4 has-text-warning">Recherche d'un consultant</p>
              <p>Une fois que nous connaissons le besoin du client nous cherchons un étudiant apte à réaliser l'étude</p>
            </div>
          </FadeUpComponent>
        </div>
        <div className="timeline-item is-primary">
          <div className="timeline-marker is-link is-image is-32x32"></div>
          <FadeUpComponent>
            <div className="timeline-content">
              <p className="title is-4 has-text-warning">Signature de la convention d'étude</p>
              <p>Une fois le devis accepté et l'intervenant trouvé, nous générons la convention d'étude attestant du début de l'étude</p>
            </div>
          </FadeUpComponent>
        </div>
        <div className="timeline-item is-primary">
          <div className="timeline-marker is-link is-image is-32x32"></div>
          <FadeUpComponent>
            <div className="timeline-content">
              <p className="title is-4 has-text-warning">Réalisation de l'étude</p>
              <p>La convention d'étude signée le consultant commence le travail en utilisant les méthodes agiles</p>
            </div>
          </FadeUpComponent>
        </div>
        <div className="timeline-item is-primary">
          <div className="timeline-marker is-link is-image is-32x32"></div>
          <FadeUpComponent>
            <div className="timeline-content">
              <p className="title is-4 has-text-warning">Avenant de délai ou contenu</p>
              <p>Si l'on se rend compte que le besoin du client n'avait pas été bien cerné, il est possible de modifier la convention d'étude afin de coller au mieux aux besoins du client</p>
            </div>
          </FadeUpComponent>
        </div>
      </div>
    )
}


export default PresentationEtude
