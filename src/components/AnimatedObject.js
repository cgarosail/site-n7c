import React from "react";
import posed from "react-pose";

const AnimatedText = posed.div({
  open: {
    y: 0,
    opacity: 1,
    delay: ({delaiPlus}) => (500 + 500*0.05*delaiPlus),
    transition: {
        opacity: { ease: 'easeOut', duration: 300 },
        default: { ease: 'easeOut', duration: 1200 }
    }
  },
  closed: {
    y: 100,
    opacity: 0,
    transition: { duration: 700 }
  },
  props: {delaiPlus: 0}
});

function AnimatedObject(props) {

  return (
  <div>
      <AnimatedText className={props.className} style={{marginBottom: "1%"}} delaiPlus={props.delaiPlus} pose={props.etat}>{props.children}</AnimatedText>
  </div>
  );
}
export default AnimatedObject