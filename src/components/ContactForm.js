import React from "react"
import * as emailjs from 'emailjs-com'


class ContactForm extends React.PureComponent {
    constructor() {
        super()
        emailjs.init("user_D0AGmYyvQ93EWLsEs47jC");
        this.state = {
            nom: "",
            email: "",
            sujet: "",
            message: "",
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    
    handleChange(event) {
        const {name, value, type, checked} = event.target
        type === "checkbox" ? this.setState({ [name]: checked }) : this.setState({ [name]: value })
    }

    async handleSubmit(event) {
        event.preventDefault();
        console.log("On submit");
        emailjs.send(
            'gmail',
            "template_GQlUOuqk",
            {   from_name:this.state.nom,
                to_name:"Camille Garo Sail",
                from_mail:this.state.email,
                from_subject:this.state.sujet,
                message_html:this.state.message},
                )


        this.setState ({
            nom: "",
            email: "",
            sujet: "",
            message: "",
        })
    }
    render() {

        return (
            <div className="hero">
                <div className="hero-head">
                    <h1 className="title is-3 has-text-warning">Nous contacter par mail</h1>
                </div>
                <div className="hero-body contact">
                    <div className="container">

                        <form onSubmit={this.handleSubmit}>
                            <div className="field">
                                <label className="label is-medium">Nom</label>
                                <div className="control">
                                    <input 
                                        className="input is-medium"
                                        type="text" 
                                        value={this.state.nom} 
                                        name="Nom" 
                                        placeholder="Jean Dupond" 
                                        onChange={this.handleChange} 
                                        />
                                </div>
                            </div>


                            <div className="field">
                                <label className="label is-medium">Email</label>
                                <div className="control">
                                    <input 
                                        className="input is-medium"
                                        type="text" 
                                        value={this.state.email} 
                                        name="Email" 
                                        placeholder="JeanDupond@mail.com" 
                                        onChange={this.handleChange} 
                                        />
                                </div>
                            </div>

                            <div className="field">
                                <label className="label is-medium">Sujet</label>
                                <div className="control">

                                    <input 
                                        className="input is-medium"
                                        type="text" 
                                        value={this.state.sujet} 
                                        name="sujet" 
                                        placeholder="Demande de devis" 
                                        onChange={this.handleChange} 
                                        />
                                </div>
                            </div>

                            <div className="field">
                                <label className="label is-medium">Message</label>
                                <div className="control">

                                <textarea 
                                    className="textarea is-medium"
                                    type="textarea" 
                                    value={this.state.message} 
                                    name="message"
                                    placeholder="Votre message"
                                    onChange={this.handleChange} 
                                    />
                                </div>
                            </div>
                            
                            <br/>

                            <input className="button is-link" type="submit" value="Submit" />
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default ContactForm
