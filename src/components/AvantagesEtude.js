import React from 'react'


function AvantagesEtude() {
    return (
        <section className="hero">

        <div className="hero-body">
        <div className="container">
          <section className="hero is-transparent text-invert">
            <div className="hero-head">
              <div className="container">
                <h1 className="title is-size-3 has-text-warning has-text-centered">
                  Ce que la JE peut vous apporter en tant qu'étudiant
                </h1>
              </div>
            </div>
            <div className="hero-body">
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse vitae elit quis tortor posuere efficitur faucibus quis est. In hac habitasse platea dictumst. Nullam in arcu quis velit feugiat commodo. Sed eu sollicitudin velit. Nulla lacinia feugiat orci vel aliquam. Etiam hendrerit rutrum consequat. Morbi id bibendum enim. Suspendisse interdum magna nec blandit venenatis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Morbi tempor urna erat, at tempus elit venenatis et. Sed finibus tortor ac ante luctus, vitae gravida ante mattis.
                    <br/>
                    Nullam nec magna in tellus ornare rutrum. Vestibulum eget pharetra magna. Sed facilisis, diam non feugiat porttitor, metus purus sodales felis, at placerat nulla massa in risus. Mauris nec cursus massa, sed consequat orci. Vivamus ipsum orci, aliquet nec tempor et, cursus vel neque. Cras vehicula tincidunt erat quis accumsan. Praesent vulputate libero eu lorem bibendum rhoncus. Duis hendrerit molestie nibh, non egestas nunc convallis vel. Aenean quis elementum odio. Phasellus risus arcu, tempor non mollis eu, cursus eu felis. Ut porttitor eros id lorem sodales pharetra. Nunc arcu nunc, sodales ac tempus ac, mattis ac lorem.
                </p>
            </div>
          </section>
        </div>
      </div>
    </section>
    )
}

export default AvantagesEtude