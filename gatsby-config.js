module.exports = {
  siteMetadata: {
    title: `N7 Consulting`,
    navbar: {
      title: 'N7 Consulting',
      options: 
      [
        {
          name: 'Vous êtes ?',
          deroulant: true,
          subs : 
          [
            {
              name: "Un professionel",
              link : "/entreprise"
            },

            {
              name: 'Un étudiant',
              link: "/etudiant"
            }
          ] 
        },

        {
          name: "L'équipe",
          deroulant: false,
          link: '/n7crew',
          dl: false
        },
        {
          name: "La plaquette",
          deroulant: false,
          link: '/site-n7c/plaquette.png',
          dl: true
        },
        {
          name: "Actualités",
          deroulant: false,
          link: '/news',
          dl: false
        },
        {
          name: "Nous Contacter",
          deroulant: false,
          link: '/contact',
          dl: false
        }
      ]
      
    },
    author: `@n7consulting`,
    pages: {
        Accueil: {
          PresentationJE: {
            titre : "Présentation de la Junior et du Mouvement",
            paragraphes : [
              {
                texte : "Ceci est un paragraphe, dans cet exemple il n'y a pas de titre au paragraphe mais on peut en rajouter un ! Vestibulum eget lacinia quam. Praesent ac viverra ligula, vel vulputate nunc. Donec ac dolor a elit sagittis luctus. Duis venenatis porttitor feugiat. In erat diam, posuere vitae viverra vitae, pulvinar in mauris. Donec blandit lacus vel varius suscipit. Maecenas lacus ipsum, finibus nec dapibus eget, placerat nec nibh."
              },
              {
                titre: "Paragraphe avec titre",
                texte : "Vivamus nulla nunc, aliquam nec suscipit nec, aliquam imperdiet lorem. Nunc sodales sem ac ipsum condimentum, in maximus tortor molestie. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut a lacus leo. Morbi in dui lacus. Suspendisse risus libero, porttitor quis neque a, efficitur gravida lectus. Ut mattis velit et erat interdum condimentum. Donec volutpat quam rhoncus justo sodales varius. Nunc non porta risus."
              }
            ]
          },
          Statistiques: {
            titre: "Les Chiffres sur l'année",
            stats : [
              {
                titre: "Satisfaction Client",
                valeur: "99.9%"
              },
              {
                titre: "Etudes Réalisées",
                valeur: "50"
              },
              {
                titre: "Chiffre D'affaire",
                valeur: "100k €"
              },
              {
                titre: "Une Stat de Ouf",
                valeur: "3 000"
              }
            ]
          },
          Competences : {
            titre: "Nos domaines de compétence",
            paragraphe: "Praesent nec augue tellus. Proin semper augue vitae ornare vehicula. Sed congue ultrices ante, non tempor velit malesuada non. Vivamus ac sem a leo placerat pharetra vitae sed massa. Sed tempor id ipsum a euismod. Suspendisse malesuada blandit porttitor. Morbi odio dolor, tincidunt at nibh finibus, rhoncus molestie leo. Mauris gravida, enim non elementum cursus, elit justo porta purus, vitae varius lorem ante a turpis. Aenean feugiat eros a libero iaculis, non convallis enim pharetra.",
            SN: {
              titre : "Sciences du Numérique",
              texte: "C'est la meilleure filière tout simplement"
            },
            HYDRO: {
              titre: "Hydrolique",
              texte: "Ils jouent avec de l'eau, et n'auront pas de boulot"
            },
            EEEA: {
              titre: "Energie, Electronique E Automatique",
              texte: "Ils en chient bien cette année quand même..."
            }
          },
          Etudes: {
            titre: "Presentation d'études interressantes",
            paragraphe: "Ce serait juste un paragraphe qui pose les choses",
            etudes : [
              {
                titre: "Etude 1",
                texte: "Cette étude c'était le feu genre vraiment on a fait des trucs de ouf !"
              },
              {
                titre: "Etude 2",
                texte: "Celle là c'était vraiment interressant et en plus on en fait pleins des comme ça, on maitrise le sujet !"
              },
              {
                titre: "Etude 3",
                texte: "Celle là c'est une hydro ! Oh wait on en pas encore fait... Ah bah dommage... Nan mais ça viendra !"
              }
            ]
          }

      }
    }
  },

  pathPrefix: '/site-n7c',
  plugins: [
    `gatsby-plugin-react-helmet`,
    'gatsby-plugin-sass',
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `N7 Consulting`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/Logo_blanc.png`, // This path is relative to the root of the site.
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
